var express = require("express"),
    app = express(),
    http = require("http").Server(app),
    io = require("socket.io")(http),
    path = require("path"),
    mongoose = require('mongoose'),
    bodyParser = require("body-parser"),
    Schema = mongoose.Schema;

var roomID = "abc123",
    Global = {_UsersList : Array()};

mongoose.connect("mongodb://localhost/chat");


var UserSchema = new Schema({
        account: String,
        pwd: String,
        Nickname:String
    }),
    MsgLogSchema = new Schema({
        msg: String
    });

app.set('views', __dirname + '/views');
app.set('staic', __dirname+'/static');
app.set("view engine", "jade");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use('/public', express.static(__dirname + "/public"));
var users = mongoose.model("Users", UserSchema),
    MsgLog = mongoose.model("MsgLog", MsgLogSchema);
var Tomas = new users({
    account: "Simon",
    pwd: "123456",
    Nickname: "Simon"
});

app.get("/", function(req, res){

    res.render("index", {"UserList", Global._UserList});
});

app.route("/login/")
    .get(function(req, res){
        res.render("login");
    })
    .post(function(req, res){
        users.find({account:req.body.account}, function(err, result){
            if(err){
            console.log(err);
            }
            if(result){
                console.log("Login");
            }else{
                console.log("Fail");
            }
        });
        res.render("login")
    });

app.route("/user/create/")
    .get(function(req, res){
        res.render("user/create");
    })
    .post(function(req, res){
        //check and write into mongodb
    });

app.route("/user/show/:uid")
    .get(function(req, res){
        users.findOne({"account":req.params.uid}, function(err, result){
            console.log(result);
            res.render("user/show", {"account":result.account, "nickname":result.Nickname});
        });
    });

io.on('connection', function(socket){

    Global._UsersList.push({name:"Tomas", uid:socket.id});

    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
        MsgLog.create({"msg": msg}, function(err){
            if(err) { console.log(err); }
        });
    });

    socket.on("disconnect", function(){
        io.emit("a user disconnected");
        Global._UsersList.pop({name:"Tomas", uid:socket.id});
    });
});

http.listen(3000, function(){
    console.log("listening on *:3000");
});
